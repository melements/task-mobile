import { combineReducers } from 'redux';

import home from 'app/components/home/home.reducer';
import results from 'app/components/results/results.reducer';

const reducers = combineReducers({ home, results });

export default (state, action) => reducers(state, action);
