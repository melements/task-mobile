import React from 'react';
import { createStackNavigator } from 'react-navigation';

import { screens } from 'config/config';
import HomeScreen from 'app/components/home/home.container';
import ResultsScreen from 'app/components/results/results.container';
import Header from 'app/components/header/header.component';

const routes = createStackNavigator(
  {
    HomeScreen,
    ResultsScreen,
  },
  {
    initialRouteName: screens.home,
    defaultNavigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} />,
    }),
    cardStyle: {
      backgroundColor: '#f5f5f5',
    },
  },
);

export default routes;
