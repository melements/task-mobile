const BASE_URL = 'https://api.github.com';

const titles = {
  favourites: 'favourites',
  results: 'results',
};

const screens = {
  home: 'HomeScreen',
  results: 'ResultsScreen',
};

const languages = {
  javascript: 'JavaScript',
  java: 'Java',
  python: 'Python',
  ruby: 'Ruby',
  go: 'Go',
  bash: 'Shell',
  typescript: 'TypeScript',
  groovy: 'Groovy',
  scala: 'Scala',
  kotlin: 'Kotlin',
  php: 'PHP',
  cpp: 'C++',
  c: 'C',
};

export {
  BASE_URL,
  titles,
  screens,
  languages,
};
