import { BASE_URL } from 'config/config';

class Api {
  constructor() {
    this.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    this.baseUrl = BASE_URL;
  }

  static getRouteWithParams(route, params) {
    const tmp = Object.keys(params).map(key => `${key}=${params[key]}`);
    return `${route}?${tmp.join('&')}`;
  }

  get(route, params) {
    let newRoute = route;
    if (params) {
      newRoute = Api.getRouteWithParams(route, params);
    }
    return this.xhr(newRoute, null, 'GET');
  }

  async xhr(route, params, method) {
    const url = `${this.baseUrl}${route}`;
    const options = {
      method,
      url,
      headers: this.headers,
    };

    if (method === 'GET') {
      options.body = params;
    } else {
      options.body = JSON.stringify(params);
    }

    return fetch(url, options)
      .then((response) => {
        switch (response.status) {
          case 200:
          case 201:
            return response.json();
          case 204:
            return Promise.resolve();
          case 422: {
            return response.json().then((data) => {
              const message = data.message || 'An internal error has occured';
              console.error(message);
              return Promise.reject(new Error(message));
            });
          }
          case 401:
            return Promise.reject();
          default: {
            const message = response.statusText || 'An internal error has occured';
            console.error(message);
            return Promise.reject(new Error(response.statusText));
          }
        }
      })
      .catch((e) => {
        const message = 'An internal error has occured';
        console.error(message);
        return Promise.reject(e);
      });
  }
}

export default new Api();
