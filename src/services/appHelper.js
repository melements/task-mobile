class AppHelper {
  createQuery = (params) => {
    const currentDate = new Date();
    currentDate.setFullYear(currentDate.getFullYear() - 1); // set last year
    const lastYear = currentDate.toISOString().slice(0, 10); // YYYY-MM-DD

    const query = {
      q: `${params.name}+stars:>10+pushed:>=${lastYear}+language:${params.language}`, // stargazers_count>10 && updated_at>lastYear
      sort: 'stars',
      per_page: 20,
      page: 1,
    };

    return query;
  }

  guid = () => {
    const s4 = () => Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
    return `${s4() + s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
  }
}

export default new AppHelper();
