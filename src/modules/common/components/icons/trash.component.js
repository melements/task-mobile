import React from 'react';
import Svg, { Path } from 'react-native-svg';

class TrashIcon extends React.PureComponent {
  render() {
    return (
      <Svg width={20} height={20}>
        <Path d="M17 2h-3.5l-1-1h-5l-1 1H3v2h14zM4 17a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V5H4z" fill="#ffffff" />
      </Svg>
    );
  }
}

export default TrashIcon;
