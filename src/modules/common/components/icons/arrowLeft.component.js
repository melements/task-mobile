import React from 'react';
import Svg, { Path } from 'react-native-svg';

class ArrowLeftIcon extends React.PureComponent {
  render() {
    return (
      <Svg width={25} height={25} viewBox="0 0 1000 1000">
        <Path d="M402 108c13.5 0 25.1 4.8 34.6 14.4 9.6 9.6 14.4 21.1 14.4 34.6 0 13.8-4.8 25.4-14.5 34.8L177.3 451H941c13.5 0 25.1 4.8 34.6 14.4S990 486.5 990 500s-4.8 25.1-14.4 34.6C966 544.2 954.5 549 941 549H177.3l259.2 259.2c9.7 9.4 14.5 21.1 14.5 34.8 0 13.5-4.8 25.1-14.4 34.6-9.6 9.6-21.1 14.4-34.6 14.4-13.8 0-25.4-4.7-34.8-14.2l-343-343C14.7 524.9 10 513.3 10 500c0-13.3 4.7-24.9 14.2-34.8l343-343c9.7-9.5 21.3-14.2 34.8-14.2z" fill="#2a6a86" />
      </Svg>
    );
  }
}

export default ArrowLeftIcon;
