import React from 'react';
import Svg, { Path } from 'react-native-svg';

class PlusIcon extends React.PureComponent {
  render() {
    return (
      <Svg width={15} height={15} viewBox="0 -256 1792 1792">
        <Path
          d="M1613.017 568.95v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"
          fill="#31a6cd"
        />
      </Svg>
    );
  }
}

export default PlusIcon;
