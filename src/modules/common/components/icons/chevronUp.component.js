import React from 'react';
import Svg, { Path } from 'react-native-svg';

class ChevronDown extends React.PureComponent {
  render() {
    return (
      <Svg
        width={24}
        height={16}
        fill="none"
        stroke="#77c6e0"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <Path d="M18 15l-6-6-6 6" />
      </Svg>
    );
  }
}

export default ChevronDown;
