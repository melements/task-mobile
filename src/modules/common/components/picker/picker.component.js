import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  Picker,
} from 'react-native';
import { languages } from 'config/config';

import ActionSheet from 'common/components/actionSheet/actionSheet.component';

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
  },
  inputPlaceholder: {
    color: '#acacac',
  },
  inputContainer: {
    marginBottom: 10,
    borderBottomColor: '#0095c5',
    borderBottomWidth: 2,
  },
  input: {
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
    fontSize: 24,
  },
  button: {
    marginBottom: 10,
    fontWeight: '500',
  },
  underline: {
    borderBottomColor: '#0095c5',
    borderBottomWidth: 2,
    marginBottom: 20,
  },
});

class CustomPicker extends React.Component {
  constructor(props) {
    super(props);
    if (Platform.OS === 'android') {
      this.languagesArray = Object.keys(languages).map(item => ({
        value: item,
        displayName: languages[item],
      }));
      this.languagesArray.unshift({
        value: '',
        displayName: '',
      });
    }
    if (Platform.OS === 'ios') {
      this.languagesArray = Object.keys(languages).map(item => languages[item]);
      this.languagesArray.push('Cancel');
    }
  }

  render() {
    const { language, handlePickerChange } = this.props;

    if (Platform.OS === 'ios') {
      return (
        <ActionSheet
          language={language}
          options={this.languagesArray}
          handlePickerChange={handlePickerChange}
        />);
    }

    return (
      <View style={styles.underline}>
        <Text style={styles.inputPlaceholder}>Language</Text>
        <Picker
          selectedValue={language}
          style={styles.picker}
          onValueChange={handlePickerChange}
          mode="dropdown"
        >
          {this.languagesArray.map(item => (
            <Picker.Item
              label={item.displayName}
              value={item.value}
              key={item.displayName}
            />
          ))
          }
        </Picker>
      </View>
    );
  }
}

CustomPicker.propTypes = {
  language: PropTypes.string.isRequired,
  handlePickerChange: PropTypes.func.isRequired,
};

export default CustomPicker;
