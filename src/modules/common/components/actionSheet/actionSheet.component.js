import React from 'react';
import {
  ActionSheetIOS,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
  },
  inputPlaceholder: {
    color: '#acacac',
  },
  inputContainer: {
    marginBottom: 10,
    borderBottomColor: '#0095c5',
    borderBottomWidth: 2,
  },
  input: {
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
    fontSize: 24,
    height: 30,
  },
});

const ActionSheet = ({ language, options, handlePickerChange }) => {
  const showActionSheet = () => {
    ActionSheetIOS.showActionSheetWithOptions({
      options,
      cancelButtonIndex: options.length - 1,
    },
      (buttonIndex) => {
        if (buttonIndex === options.length - 1) {
          return;
        }
        handlePickerChange(options[buttonIndex]);
      });
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.inputContainer} onPress={showActionSheet}>
        <Text style={styles.inputPlaceholder}>Language</Text>
        <Text style={styles.input}>
          {language}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

ActionSheet.propTypes = {
  language: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  handlePickerChange: PropTypes.func.isRequired,
};

export default ActionSheet;
