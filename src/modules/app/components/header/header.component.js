import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { screens, titles } from 'config/config';
import ArrowLeftIcon from 'common/components/icons/arrowLeft.component';

const Logo = require('assets/Octocat.png');

const styles = StyleSheet.create({
  header: {
    height: 120,
    marginBottom: 5,
    backgroundColor: '#4dc5f8',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 3,
    },
    shadowRadius: 5,
    elevation: 8,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 40,
    marginLeft: 40,
    marginRight: 40,
  },
  titleContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  iconButton: {
    alignSelf: 'center',
    marginRight: 10,
  },
  imageButton: {
    width: 20,
    height: 20,
  },
  title: {
    color: '#2a6a86',
    fontSize: 24,
    fontWeight: '500',
  },
  image: {
    width: 70,
    height: 70,
  },
});

export default class Header extends Component {
  pressHandler = () => {
    const { navigation: { goBack } } = this.props;
    return goBack();
  }

  render() {
    const { navigation } = this.props;
    const isResultScreen = navigation.state.routeName === screens.results;
    const title = isResultScreen ? titles.results : titles.favourites;
    return (
      <View style={styles.header}>
        <View style={styles.container}>
          <View style={styles.titleContainer}>
            {isResultScreen
              && (
                <TouchableOpacity
                  onPress={this.pressHandler}
                  style={styles.iconButton}
                >
                  <ArrowLeftIcon />
                </TouchableOpacity>
              )
            }
            <Text style={styles.title}>
              {`Search ${title}`}
            </Text>
          </View>
          <Image
            style={styles.image}
            source={Logo}
          />
        </View>
      </View>
    );
  }
}

Header.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    state: PropTypes.shape({}).isRequired,
  }).isRequired,
};
