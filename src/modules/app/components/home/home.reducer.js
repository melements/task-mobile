import {
  SET_INPUT_VALUE,
  ADD_FAVOURITE,
  REMOVE_FAVOURITE,
  SET_LANGUAGE,
} from './home.actions';

const initialState = {
  name: '',
  language: '',
  favourites: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_INPUT_VALUE: {
      return {
        ...state,
        name: payload,
      };
    }
    case SET_LANGUAGE: {
      return {
        ...state,
        language: payload,
      };
    }
    case ADD_FAVOURITE: {
      return {
        ...state,
        favourites: [
          ...state.favourites,
          payload,
        ],
      };
    }
    case REMOVE_FAVOURITE: {
      return {
        ...state,
        favourites: state.favourites.filter((item, index) => index !== payload),
      };
    }
    default:
      return state;
  }
};
