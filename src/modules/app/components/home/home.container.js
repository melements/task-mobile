import { connect } from 'react-redux';
import HomeScreen from './home.component';
import {
  setInputValue,
  addFavourite,
  removeFavourite,
} from './home.actions';

const mapStateToProps = ({ home }) => ({
  name: home.name,
  language: home.language,
  favourites: home.favourites,
});


const mapDispatchToProps = {
  setInputValue,
  addFavourite,
  removeFavourite,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
