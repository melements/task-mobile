import React, { Component } from 'react';
import {
  ScrollView,
  Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import { screens } from 'config/config';

import SearchBox from 'app/components/searchBox/searchBox.container';
import Favourite from 'app/components/favourite/favourite.component';

class HomeScreen extends Component {
  favouriteClickHandler = (favourite) => {
    const { navigation } = this.props;
    const routeName = screens.results;
    const params = favourite;
    navigation.navigate({ routeName, params });
  }

  deleteConfirmDialog = (favourite, index) => {
    const { removeFavourite } = this.props;
    Alert.alert(
      'Alert',
      `Do you want to remove ${favourite.name}?`,
      [
        { text: 'Cancel', onPress: () => { }, style: 'cancel' },
        { text: 'Remove', onPress: () => removeFavourite(index) },
      ],
      { cancelable: false },
    );
  }

  render() {
    const { favourites, language } = this.props;

    return (
      <ScrollView>
        <SearchBox
          language={language}
          addToListHandler={this.addToListHandler}
        />
        {favourites.map((favourite, index) => (
          <Favourite
            pressHandler={() => this.favouriteClickHandler(favourite)}
            key={favourite.id}
            favourite={favourite}
            deleteHandler={() => this.deleteConfirmDialog(favourite, index)}
          />
        ))}
      </ScrollView>
    );
  }
}

HomeScreen.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    state: PropTypes.shape({}).isRequired,
  }).isRequired,
  language: PropTypes.string.isRequired,
  favourites: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  })).isRequired,
  removeFavourite: PropTypes.func.isRequired,
};

export default HomeScreen;
