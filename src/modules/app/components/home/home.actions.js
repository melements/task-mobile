const namespace = 'HOME';

const SET_INPUT_VALUE = `SET_INPUT_VALUE_${namespace}`;
const SET_LANGUAGE = `SET_LANGUAGE_${namespace}`;
const ADD_FAVOURITE = `ADD_FAVOURITE_${namespace}`;
const REMOVE_FAVOURITE = `REMOVE_FAVOURITE_${namespace}`;

const setInputValue = payload => ({
  type: SET_INPUT_VALUE,
  payload,
});

const setLanguage = payload => ({
  type: SET_LANGUAGE,
  payload,
});

const addFavourite = payload => ({
  type: ADD_FAVOURITE,
  payload,
});

const removeFavourite = payload => ({
  type: REMOVE_FAVOURITE,
  payload,
});

export {
  SET_INPUT_VALUE,
  SET_LANGUAGE,
  ADD_FAVOURITE,
  REMOVE_FAVOURITE,
};

export {
  setInputValue,
  setLanguage,
  addFavourite,
  removeFavourite,
};
