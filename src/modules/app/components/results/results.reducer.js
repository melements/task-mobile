import {
  RESET_STATE,
  SET_IS_LOADING,
  HANDLE_RESPONSE,
} from './results.actions';

const initialState = {
  items: [],
  nextPage: 1,
  totalCount: 0,
  isLoading: true,
  query: {},
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case RESET_STATE: {
      return {
        ...initialState,
      };
    }
    case SET_IS_LOADING: {
      return {
        ...state,
        isLoading: payload,
      };
    }
    case HANDLE_RESPONSE: {
      return {
        ...state,
        items: [...state.items, ...payload.items],
        nextPage: state.nextPage + 1,
        totalCount: payload.total_count,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};
