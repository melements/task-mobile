import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  View,
} from 'react-native';

import ResultCard from 'app/components/resultCard/resultCard.component';

const styles = StyleSheet.create({
  container: {
    margin: 10,
    flex: 1,
  },
  loadingContainer: {
    height: 100,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  text: {
    fontSize: 18,
    marginBottom: 10,
  },
  chip: {
    alignSelf: 'flex-start',
    paddingVertical: 5,
    paddingHorizontal: 10,
    fontSize: 12,
    backgroundColor: '#fce0b6',
    borderRadius: 21,
  },
});

class ResultsScreen extends Component {
  componentDidMount() {
    const { getItems, navigation, nextPage } = this.props;
    getItems(navigation.state.params, nextPage);
  }

  componentWillUnmount() {
    const { reset } = this.props;
    reset();
  }

  scrollHandler = ({ nativeEvent }) => {
    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
      const paddingToBottom = 50;
      return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
    };
    const {
      nextPage,
      totalCount,
      isLoading,
      getItems,
      navigation,
    } = this.props;

    if (isCloseToBottom(nativeEvent) && !isLoading) { // block multiple requests for same page
      if (totalCount - (nextPage - 1) * 20 > 0) { // check if there are any elements left
        getItems(navigation.state.params, nextPage);
      }
    }
  }

  render() {
    const { items, isLoading } = this.props;
    return (
      <ScrollView
        style={styles.container}
        onScroll={this.scrollHandler}
        scrollEventThrottle={400}
      >
        <FlatList
          data={items}
          renderItem={({ item }) => <ResultCard item={item} />}
          keyExtractor={item => item.id.toString()}
        />
        {isLoading && (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color="#a7e0fc" />
          </View>
        )}
      </ScrollView>
    );
  }
}

ResultsScreen.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    state: PropTypes.shape({}).isRequired,
  }).isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      description: PropTypes.string,
      languages_url: PropTypes.string,
    }),
  ).isRequired,
  isLoading: PropTypes.bool.isRequired,
  nextPage: PropTypes.number.isRequired,
  totalCount: PropTypes.number.isRequired,
  reset: PropTypes.func.isRequired,
  getItems: PropTypes.func.isRequired,
};

export default ResultsScreen;
