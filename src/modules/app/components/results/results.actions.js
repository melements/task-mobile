import Api from 'services/api';

const namespace = 'RESULTS';

const RESET_STATE = `RESET_STATE_${namespace}`;
const SET_IS_LOADING = `SET_IS_LOADING_${namespace}`;
const HANDLE_RESPONSE = `HANDLE_RESPONSE_${namespace}`;

const createQuery = (params, nextPage) => {
  const currentDate = new Date();
  currentDate.setFullYear(currentDate.getFullYear() - 1); // set last year
  const lastYear = currentDate.toISOString().slice(0, 10); // YYYY-MM-DD

  const query = {
    q: `${params.name}+stars:>10+pushed:>=${lastYear}+language:${params.language}`, // stargazers_count>10 && updated_at>lastYear
    sort: 'stars',
    per_page: 20,
    page: nextPage,
  };

  return query;
};

const reset = () => ({
  type: RESET_STATE,
});

const getItems = (params, nextPage) => async (dispatch) => {
  dispatch({
    type: SET_IS_LOADING,
    payload: true,
  });

  try {
    const res = await Api.get('/search/repositories', createQuery(params, nextPage));
    dispatch({
      type: HANDLE_RESPONSE,
      payload: res,
    });
  } catch (err) {
    console.log(err);
  }
};

export {
  RESET_STATE,
  SET_IS_LOADING,
  HANDLE_RESPONSE,
};

export {
  reset,
  getItems,
};
