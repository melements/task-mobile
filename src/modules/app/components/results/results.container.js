import { connect } from 'react-redux';
import ResultsScreen from './results.component';
import {
  reset,
  getItems,
} from './results.actions';

const mapStateToProps = ({ results }) => ({
  items: results.items,
  nextPage: results.nextPage,
  totalCount: results.totalCount,
  isLoading: results.isLoading,
});


const mapDispatchToProps = {
  reset,
  getItems,
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultsScreen);
