import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
  card: {
    marginVertical: 5,
    marginHorizontal: 10,
    backgroundColor: '#ffffff',
    shadowOpacity: 0.2,
    shadowOffset: {
      height: 1,
    },
    shadowRadius: 2,
    elevation: 5,
  },
});

const Card = ({ children }) => (
  <View style={styles.card}>
    {children}
  </View>
);

Card.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};


export default Card;
