import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';

import Api from 'services/api';
import BASE_URL from 'config/config';

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  chip: {
    alignSelf: 'flex-start',
    paddingVertical: 5,
    paddingHorizontal: 10,
    fontSize: 12,
    backgroundColor: '#a7e0fc',
    borderRadius: 21,
    marginRight: 5,
    marginBottom: 10,
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
  },
});

class Dropdown extends React.PureComponent {
  state = {
    languages: [],
    totalBytes: 0,
  };

  componentDidMount() {
    const { languagesUrl } = this.props;
    const url = languagesUrl.replace(BASE_URL, '');
    Api.get(url).then((res) => {
      let totalBytes = 0;
      const languages = [];
      Object.keys(res).forEach((key) => {
        totalBytes += res[key];
        languages.push({
          name: key,
          bytes: res[key],
        });
      });

      this.setState({
        languages,
        totalBytes,
      });
    });
  }

  toPercent = (language) => {
    const { totalBytes } = this.state;
    return Math.round((language.bytes / totalBytes) * 100);
  }


  render() {
    const { languages } = this.state;
    return (
      <View style={styles.container}>
        {
          !languages.length && (
            <View style={styles.loader}>
              <ActivityIndicator size="small" color="#a7e0fc" />
            </View>
          )
        }
        {languages.map(language => (
          <View key={language.name} style={styles.chip}>
            <Text>{`${language.name} ${this.toPercent(language)}%`}</Text>
          </View>
        ))}
      </View>
    );
  }
}

Dropdown.propTypes = {
  languagesUrl: PropTypes.string.isRequired,
};

export default Dropdown;
