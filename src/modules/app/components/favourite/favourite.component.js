import React from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Swipeout from 'react-native-swipeout';
import Card from 'app/components/card/card.component';
import DeleteButton from 'app/components/deleteButton/deleteButton.component';

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
    marginHorizontal: 15,
  },
  name: {
    fontSize: 18,
    marginLeft: 10,
    marginBottom: 10,
  },
  chip: {
    alignSelf: 'flex-start',
    paddingVertical: 5,
    paddingHorizontal: 10,
    fontSize: 12,
    backgroundColor: '#a7e0fc',
    borderRadius: 21,
  },
});

const Favourite = ({ favourite, deleteHandler, pressHandler }) => {
  const { name, language } = favourite;
  const swipeoutButton = [
    {
      component: <DeleteButton />,
      backgroundColor: '#0095c5',
      onPress: deleteHandler,
    },
  ];

  return (
    <Card>
      <Swipeout
        right={swipeoutButton}
        autoClose
        buttonWidth={250}
        backgroundColor="transparent"
      >
        <TouchableOpacity onPress={pressHandler}>
          <View style={styles.container}>
            <Text style={styles.name}>{name}</Text>
            <View style={styles.chip}>
              <Text>{language}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </Swipeout>
    </Card>
  );
};

Favourite.propTypes = {
  favourite: PropTypes.shape({
    name: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
  }).isRequired,
  deleteHandler: PropTypes.func.isRequired,
  pressHandler: PropTypes.func.isRequired,
};

export default Favourite;
