import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TextInput,
  Text,
} from 'react-native';

import AppHelper from 'services/appHelper';
import Picker from 'common/components/picker/picker.component';
import Card from 'app/components/card/card.component';
import AddToListButton from 'app/components/addToListButton/addToListButton.component';

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  inputPlaceholder: {
    color: '#acacac',
  },
  inputContainer: {
    marginBottom: 10,
  },
  inputCounter: {
    color: '#acacac',
    alignSelf: 'flex-end',
  },
  input: {
    borderBottomColor: '#0095c5',
    borderBottomWidth: 2,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
    fontSize: 24,
  },
});

class SearchBox extends React.PureComponent {
  state = {
    name: '',
    language: '',
  };

  handleInputChange = (name) => {
    if (name.length <= 50) {
      this.setState({ name });
    }
  }

  handlePickerChange = (language) => {
    this.setState({ language });
  }

  addToListHandler = () => {
    const { addFavourite } = this.props;
    const { name, language } = this.state;
    addFavourite({
      name,
      language,
      id: AppHelper.guid(),
    });
  }

  render() {
    const {
      name,
      language,
    } = this.state;

    return (
      <Card>
        <View style={styles.container}>
          <View style={styles.inputContainer}>
            <Text style={styles.inputPlaceholder}>Repo name or description</Text>
            <TextInput
              value={name}
              style={styles.input}
              onChangeText={this.handleInputChange}
            />
            <Text style={styles.inputCounter}>{`${name.length} / 50`}</Text>
          </View>
          <Picker language={language} handlePickerChange={this.handlePickerChange} />
        </View>
        <AddToListButton value={name} language={language} addFavourite={this.addToListHandler} />
      </Card>
    );
  }
}

SearchBox.propTypes = {
  addFavourite: PropTypes.func.isRequired,
};

export default SearchBox;
