import { connect } from 'react-redux';
import {
  addFavourite,
} from 'app/components/home/home.actions';
import SearchBox from './searchBox.component';

const mapDispatchToProps = {
  addFavourite,
};

export default connect(null, mapDispatchToProps)(SearchBox);
