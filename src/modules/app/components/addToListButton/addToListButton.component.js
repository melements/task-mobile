import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

import PlusIcon from 'common/components/icons/plus.component';

const styles = StyleSheet.create({
  buttonContainer: {
    borderTopColor: '#e5e5e5',
    borderTopWidth: 1,
    padding: 12,
    paddingRight: 24,
  },
  buttonWrapper: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
  },
  buttonIcon: {
    marginRight: 5,
    alignSelf: 'center',
  },
  buttonText: {
    color: '#31a6cd',
    fontWeight: 'bold',
    fontSize: 16,
  },
  disabled: {
    opacity: 0.5,
  },
});

const AddToListButton = ({ addFavourite, value, language }) => (
  <View style={styles.buttonContainer}>
    <TouchableOpacity
      onPress={addFavourite}
      disabled={!(value && language)}
      style={[styles.buttonWrapper, !(value && language) ? styles.disabled : null]}
    >
      <View style={styles.buttonIcon}>
        <PlusIcon />
      </View>
      <Text style={styles.buttonText}>Add to list</Text>
    </TouchableOpacity>
  </View>
);

AddToListButton.propTypes = {
  value: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  addFavourite: PropTypes.func.isRequired,
};

export default AddToListButton;
