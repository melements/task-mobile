import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

import Card from 'app/components/card/card.component';
import ChevronDown from 'common/components/icons/chevronDown.component';
import ChevronUp from 'common/components/icons/chevronUp.component';
import Dropdown from 'app/components/dropdown/dropdown.component';

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  card: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  text: {
    fontSize: 18,
    marginBottom: 10,
  },
  chip: {
    alignSelf: 'flex-start',
    paddingVertical: 5,
    paddingHorizontal: 10,
    fontSize: 12,
    backgroundColor: '#fce0b6',
    borderRadius: 21,
  },
  chevron: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
  },
});

class ResultCard extends React.PureComponent {
  state = {
    isOpened: false,
  }

  handleClick = () => {
    this.setState(prevState => ({ isOpened: !prevState.isOpened }));
  }

  render() {
    const { isOpened } = this.state;
    const { item } = this.props;
    return (
      <Card>
        <View style={styles.card}>
          <Text style={styles.text}>{`${item.name} - ${item.description}`}</Text>
          <View style={styles.chip}>
            <Text>{`${item.stargazers_count} Stars`}</Text>
          </View>
          <TouchableOpacity onPress={this.handleClick} style={styles.chevron}>
            {isOpened ? <ChevronUp /> : <ChevronDown />}
          </TouchableOpacity>
          {isOpened && <Dropdown languagesUrl={item.languages_url} />}
        </View>
      </Card>
    );
  }
}

ResultCard.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
  }).isRequired,
};

export default ResultCard;
