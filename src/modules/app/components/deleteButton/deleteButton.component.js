import React from 'react';
import {
  Text,
  View,
  StyleSheet,
} from 'react-native';

import TrashIcon from 'common/components/icons/trash.component';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    paddingTop: 5,
    color: '#ffffff',
  },
});

const DeleteButton = () => (
  <View style={styles.container}>
    <TrashIcon />
    <Text style={styles.text}>DELETE</Text>
  </View>
);

export default DeleteButton;
