import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import { YellowBox } from 'react-native';

import configureStore from 'config/store';
import routes from 'config/routes';

const AppContainer = createAppContainer(routes);
const store = configureStore();

export default class App extends PureComponent {
  constructor() {
    super();
    YellowBox.ignoreWarnings(['Warning: ', 'Module', 'Debugger and device times', 'unknown call: "relay:check"']);
    console.ignoredYellowBox = [
      'Setting a timer',
      'Remote debugger',
    ];
  }

  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
